//
//  CircularProgressBar.h
//  asd
//
//  Created by Vlad on 25.09.16.
//  Copyright © 2016 qwe. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CircularProgressBar;

@protocol ProgressBarButtonDelegate <NSObject>

@required
- (void)changeButtonState:(CircularProgressBar *)pregressBar toNormalState:(BOOL)normal;

@end

@interface CircularProgressBar : UIView 
@property (nonatomic, assign) float degree;
@property (nonatomic, strong) void  (^progressBlock)(void);
@property (nonatomic, assign) int   partOfProgress;
@property (nonatomic, weak)   id    <ProgressBarButtonDelegate>delegate;

- (void)startProgress;
- (void)pauseProgress;
- (void)stopProgress;

@end
