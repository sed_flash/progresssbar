//
//  ViewController.h
//  asd
//
//  Created by Vlad on 10.09.16.
//  Copyright © 2016 qwe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *getTransform;
@property (weak, nonatomic) IBOutlet UILabel *transformedLabel;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;

@end

