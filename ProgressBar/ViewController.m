//
//  ViewController.m
//  asd
//
//  Created by Vlad on 10.09.16.
//  Copyright © 2016 qwe. All rights reserved.
//

#import "ViewController.h"
#import "CircularProgressBar.h"
#import <QuartzCore/QuartzCore.h>

#define startImage [UIImage imageNamed:@"start"]
#define pauseImage [UIImage imageNamed:@"pause"]

@interface ViewController () <ProgressBarButtonDelegate>

@property (weak, nonatomic) IBOutlet CircularProgressBar *circBar;
@property (assign, nonatomic) CGAffineTransform buttonTransform;
@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
  
    _circBar.delegate = self;
    _startBtn.adjustsImageWhenHighlighted = NO;
    _startBtn.transform = CGAffineTransformMakeRotation(90 * M_PI / 180);
    _buttonTransform = _startBtn.transform;
    
    [_startBtn addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [_startBtn addTarget:self action:@selector(buttonTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Actions

- (IBAction)getStarted:(id)sender {
    
    if (!_startBtn.selected) {
        [_circBar startProgress];
    } else {
        [_circBar pauseProgress];
    }
    
    [UIView animateWithDuration:.1 animations:^{
        _startBtn.transform = CGAffineTransformScale(_startBtn.transform, 0.3, 0.3);
        _startBtn.alpha = 0.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.1 animations:^{
            [self changeButtonState:_circBar toNormalState:NO];
            _startBtn.transform = _buttonTransform;
            _startBtn.alpha = 1.0;
        }completion:^(BOOL finished){
            NSLog(@"Finished...");
        }];
    }];
}


// По-хорошему логику кнопки, как и саму кнопку,
// нужно было вынести в отельный класс и работать в нем
#pragma mark - Start button's methods 

-(void)buttonTouchDown:(id)sender{
    UIButton *button = (UIButton *)sender;
    if(button.selected){
        [self setImageForButton:button normalStateImage:pauseImage];
    }
}

-(void)buttonTouchUp:(id)sender{
    UIButton *button = (UIButton *)sender;
    [self setImageForButton:button normalStateImage:startImage];
}

- (void)setImageForButton:(UIButton *)button normalStateImage:(UIImage *)image {
    [button setImage:image forState:UIControlStateNormal];
}

- (void)changeButtonState:(CircularProgressBar *)pregressBar toNormalState:(BOOL)normal{
    
    if (normal) {
        [self setImageForButton:_startBtn normalStateImage:startImage];
        _startBtn.selected = NO;
        return;
    }
    
    _startBtn.selected = !_startBtn.selected;
}

#pragma mark - Progress bar methods

- (IBAction)stopProgress:(id)sender {
    [_circBar stopProgress];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
