//
//  CircularProgressBar.m
//  asd
//
//  Created by Vlad on 25.09.16.
//  Copyright © 2016 qwe. All rights reserved.
//

#import "CircularProgressBar.h"

#define COMPLETE_REVOLUTION             100
#define INSIDE_RADIUS                   43
#define OUTSIDE_RADIUS                  50
#define COMPLETE_REVOLUTION_IN_RADIANS  360

#define RGBColor(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]
#define DEGREES_TO_RADIANS(degrees) (degrees * M_PI) / 180

@interface CircularProgressBar ()
@property (nonatomic, assign, getter=isRunning) BOOL running;
@end

@implementation CircularProgressBar


- (instancetype)init {
    self = [super init];
    if (self) {
        self.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    NSArray *colors = @[RGBColor(0xcdcdcd, 1),
                        RGBColor(0xf7765e, 1),
                        [UIColor whiteColor]];
    
    [self createCircleWithRadius:OUTSIDE_RADIUS
                            rect:rect
                   progressAngle:COMPLETE_REVOLUTION_IN_RADIANS
                           color:colors[0]];
    [self createCircleWithRadius:OUTSIDE_RADIUS
                            rect:rect
                   progressAngle:self.degree
                           color:colors[1]];
    [self createCircleWithRadius:INSIDE_RADIUS
                            rect:rect
                   progressAngle:COMPLETE_REVOLUTION_IN_RADIANS
                           color:colors[2]];
    
}

- (void)createCircleWithRadius:(CGFloat)radius rect:(CGRect)rect progressAngle:(CGFloat)angle color:(UIColor *)color {
    UIBezierPath *bezeirPath = [UIBezierPath bezierPath];
    [bezeirPath setLineWidth:2.0];
    [bezeirPath moveToPoint:CGPointMake(rect.size.width / 2, rect.size.height / 2)];
    [bezeirPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2) radius:radius startAngle:DEGREES_TO_RADIANS(0) endAngle:DEGREES_TO_RADIANS(angle) clockwise:YES];
    [bezeirPath closePath];
    [color setFill];
    [bezeirPath fill];
}

#pragma mark - Getters

- (float)degree {
    return _degree * 3.6;
}

- (BOOL)isRunning {
    __block BOOL blockRunning = _running;
    return blockRunning;
}

- (int)partOfProgress {
    __block int blockPartOfProgress = _partOfProgress;
    return blockPartOfProgress;
}

- (void (^)(void))progressBlock {
    
    __weak typeof(self) weakSelf = self;
    return _progressBlock = ^{
        
        weakSelf.running = YES; // индикатор работы прогресс-бара
        
        while (weakSelf.running) {
            
            // Ессли кол-во итераций больше определенного количества - остановить = 100%
            if (_partOfProgress++ >= COMPLETE_REVOLUTION) { // количество итерация
       
                [weakSelf stopProgress];
                
                continue;
            }
            
            // Установка значения прогресс-бара
            weakSelf.degree = weakSelf.partOfProgress;
            [NSThread sleepForTimeInterval:0.05];
            NSLog(@"progr: %d", weakSelf.partOfProgress); // Logs values between 0.0 and 1.0
            
            // обновить визуально прогресс-бар
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf setNeedsDisplay];
            });
        }
    };
}

#pragma mark - Manage progress bar

- (void)startProgress {
    self.partOfProgress = (self.partOfProgress >= COMPLETE_REVOLUTION) ? 0 : self.partOfProgress;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0);
    dispatch_async(queue, self.progressBlock);
}

- (void)pauseProgress {
    if (_running) {
        _running = NO;
    }
}

- (void)stopProgress {
    self.running = NO;
    self.degree = 0;
    self.partOfProgress = 0;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate changeButtonState:self toNormalState:YES];
        [self setNeedsDisplay];
    });
}

@end
