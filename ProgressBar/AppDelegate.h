//
//  AppDelegate.h
//  ProgressBar
//
//  Created by Vlad on 26.09.16.
//  Copyright © 2016 qwe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

